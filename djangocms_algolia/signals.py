from algoliasearch_django import delete_record
from cms import signals
from cms.admin.pageadmin import PageAdmin
from cms.models import Page
from djangocms_algolia.index import TitleProxy


def update_page_index(sender, instance: Page, language, **kwargs) -> None:
    for lang in instance.get_languages():
        obj_draft: TitleProxy = TitleProxy.objects.get(page=instance, language=lang)
        if obj_draft.publisher_public_id:
            obj: TitleProxy = TitleProxy.objects.get(pk=obj_draft.publisher_public_id)
            obj.save()


def remove_from_index(sender, operation: str, request, obj: Page, **kwargs):
    if operation == 'delete_page':
        for lang in obj.get_languages():
            if isinstance(obj, Page):
                # ToDo figure our why sometimes obj is not page
                try:
                    obj_draft: TitleProxy = TitleProxy.objects.get(page=obj, language=lang)
                    if obj_draft.publisher_public_id:
                        obj: TitleProxy = TitleProxy.objects.get(pk=obj_draft.publisher_public_id)
                        delete_record(obj)
                except Exception as e:
                    print(e)


signals.post_publish.connect(update_page_index, Page)
signals.post_unpublish.connect(update_page_index, Page)

# post_obj_operation signal won't work here because TitleProxy cannot be found in post_obj_operation
# (probably because the page has been already removed when signal is fired)
signals.pre_obj_operation.connect(remove_from_index, PageAdmin)
