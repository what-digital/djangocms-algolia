## 1.5

- fix the loading of signals for docker builds (where db isn't available)
- move the render functions from `djangocms_algolia.utils` to `djangocms_algolia.utils.render`
- cleanup unused code (templates, management commands, etc)
- remove django & cms version limitations from setup.py
